package net.rstvvoli.textAnalysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    private static final String PATH = "E:\\textforanalysis.txt";
    private static final String IO_EXCEPTION_ERR = "Непредвиденная ошибка.";

    public static void main(String[] args) {
        FormatText text = new FormatText();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(PATH))){

            String string;
            int spaces = 0, symbols = 0, worlds = 0;
             /*
              * Пока строки не кончатся в txt-файле, считается общее количество пробелов, символов и слов;
              */
            while ((string = bufferedReader.readLine()) != null) {
                spaces += text.numberOfSpace(string);
                symbols += text.numberOfSymbols(string);
                worlds += text.numberOfWorlds(string);
            }
            System.out.printf("Количество символов: %d \n" +
                            "Количество символов без пробела: %d \n" +
                            "Количество слов: %d \n",
                    symbols, symbols - spaces, worlds);
        } catch (IOException e) {
            System.out.println(IO_EXCEPTION_ERR);
        }
    }
}