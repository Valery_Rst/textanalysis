package net.rstvvoli.textAnalysis;

/**
 * Класс предназначен для форматирования текста - комплексу действий,
 * направленных на создание правильного оформления текста
 * соответственно тем или иным требованиям;
 */
class FormatText {

    /**
     * Метод возвращает количество пробелов в строке;
     * @param string отдельная строка текста;
     * @return количество пробелов в отдельной строке;
     */
    int numberOfSpace(String string) {
        return string.replaceAll("[^ ]", "").length();
    }

    /**
     * Метод возвращает количество символов в строке, в следствие преобразования отдельной строки
     * в массив символов;
     * @param string отдельная строка текста;
     * @return массив символов в отдельной строке;
     */
    int numberOfSymbols(String string) {
        return string.toCharArray().length;
    }

    /**
     * Метод возвращает количество слов в строке;
     * Метод replaceAll заменяет каждую подстроку данной строки, которая соответствует заданному регулярному выражению;
     * @param string отдельная строка текста;
     * @return количество слов в строке;
     */
    int numberOfWorlds(String string) {
        string = string.replaceAll("[^A-Za-z0-9]", "");
        string = string.replaceAll("-","");
        string = string.replaceAll(" {2,}", " ");

        return string.split(" ").length;
    }
}